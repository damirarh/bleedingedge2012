﻿using BE2012.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE2012.Tests.Mocks
{
    public class MockLecturesModel : ILecturesModel
    {
        public Func<List<LectureInfo>> GetLectureInfosDelegate { get; set; }
        public async Task<List<LectureInfo>> GetLectureInfos()
        {
            if (GetLectureInfosDelegate == null)
                return null;
            return await Task.Run<List<LectureInfo>>(() => GetLectureInfosDelegate());
        }
    }
}
