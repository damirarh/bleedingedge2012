﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using BE2012.ViewModels;
using BE2012.Models;
using System.Threading.Tasks;
using BE2012.Tests.Mocks;

namespace BE2012.Tests
{
    [TestClass]
    public class LecturesViewModelTest
    {
        [TestMethod]
        public async Task GroupsByTimeCorrectly()
        {
            var lectures = new List<LectureInfo>
            {
                new LectureInfo { DateStart = DateTime.Now.AddHours(9), IdLecture = 1 },
                new LectureInfo { DateStart = DateTime.Now.AddHours(9), IdLecture = 2 },
                new LectureInfo { DateStart = DateTime.Now.AddHours(11), IdLecture = 3 },
            };

            var model = new MockLecturesModel
            {
                GetLectureInfosDelegate = () => lectures
            };

            var viewModel = new LecturesViewModel(model);

            var syncTask = new Task(() => { });

            viewModel.PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName == "GroupedLectures")
                        syncTask.Start();
                };

            if (viewModel.GroupedLectures == null)
                await syncTask;

            var expectedGroups = new List<DateTime> { lectures[0].DateStart, lectures[2].DateStart };
            var expectedItems1 = new List<LectureInfo> { lectures[0], lectures[1] };
            var expectedItems2 = new List<LectureInfo> { lectures[2] };

            CollectionAssert.AreEquivalent(expectedGroups, viewModel.GroupedLectures.Groups.Select(g => g.Title).ToList());
            CollectionAssert.AreEquivalent(expectedItems1, viewModel.GroupedLectures.Groups.Single(g => g.Title == expectedGroups[0]).Items);
            CollectionAssert.AreEquivalent(expectedItems2, viewModel.GroupedLectures.Groups.Single(g => g.Title == expectedGroups[1]).Items);
        }
    }
}
