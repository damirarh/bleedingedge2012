#Bleeding Edge 2012 Session Sample Application

This is the sample application for my session at [Bleeding Edge 2012](http://www.bleedingedge.si/Conference).

Branches represent steps in the development of the application in the order I presented them during the talk. Each of them introduces a single feature or development approach.

- **1-BasicMVVM** contains only the basic application framework following the MVVM pattern with very basic model, view and view-model classes.
- **2-DesignTimeModel** adds an alternative model implementation for the designer. It uses view model locator pattern to ensure the correct model in run-time and design-time.
- **3-IoCContainer** uses MEF as an IoC container to simplify the view model locator and make it easier to maintain when the project grows.
- **4-UnitTesting** has an additional testing project in the solution for unit testing the application.
- **5-Mocking** demonstrates a simplistic approach to mocking for dependencies of the tested class.
- **6-SQLite** uses SQLite for local caching of online data for offline scenarios and improved performance.
- **7-Q42ImageCache** caches the images as well with a control from Q42.WinRT library.

The best way to take a closer look at individual branches is to make a local clone of the repository and switch to the branch you are interested in. If you don't have a Git client installed you can download the source for each of the branches if you click on the [Download](https://bitbucket.org/damirarh/bleedingedge2012/downloads) link and then switch to the *Branches* tab.

##Requirements

For the solution to compile you need the following installed:

- [WCF Data Services Tools for Windows Store Apps](http://www.microsoft.com/en-us/download/details.aspx?id=30714) (until the corresponding NuGet packages are published on NuGet, you'll need to add another package source pointing to your local disk - you can read more about it [here](http://www.damirscorner.com/WCFDataServicesToolsForWindowsStoreAppsAndNuGetPackageRestore.aspx))
- [SQLite for Windows Runtime](http://visualstudiogallery.msdn.microsoft.com/23f6c55a-4909-4b1f-80b1-25792b11639e) Visual Studio extension

NuGet package restore also [requires explicit consent from the user](http://docs.nuget.org/docs/workflows/using-nuget-without-committing-packages) to download the missing packages.
