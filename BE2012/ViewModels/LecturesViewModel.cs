﻿using BE2012.Helpers;
using BE2012.Models;
using BE2012.ODataService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace BE2012.ViewModels
{
    [Export]
    public class LecturesViewModel : ViewModelBase
    {
        private GroupCollection<LectureInfo, DateTime> groupedLectures;
        public GroupCollection<LectureInfo, DateTime> GroupedLectures
        {
            get
            {
                return groupedLectures;
            }
            set
            {
                if (groupedLectures == value)
                    return;
                groupedLectures = value;
                RaisePropertyChanged();
            }
        }

        private ILecturesModel model;

        [ImportingConstructor]
        public LecturesViewModel(ILecturesModel model)
        {
            this.model = model;

            Initialize();
        }

        private async void Initialize()
        {
            var collection = new GroupCollection<LectureInfo, DateTime>(null, l => l.DateStart);
            collection.AddItems(await model.GetLectureInfos());
            GroupedLectures = collection;
        }
    }
}
