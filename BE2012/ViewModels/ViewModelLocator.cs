﻿using BE2012.Models;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;

namespace BE2012.ViewModels
{
    public class ViewModelLocator
    {
        private static ContainerConfiguration configuration;

        static ViewModelLocator()
        {
            configuration = new ContainerConfiguration().WithPart<LecturesViewModel>();

            if (!IsDesignTime)
            {
                configuration = configuration.WithPart<CachingLecturesModel>();
            }
            else
            {
                configuration = configuration.WithPart<DesignLecturesModel>();
            }
        }

        private static bool IsDesignTime
        {
            get { return DesignMode.DesignModeEnabled; }
        }

        private CompositionHost container;

        public ViewModelLocator()
        {
            container = configuration.CreateContainer();
            container.SatisfyImports(this);
        }

        [Import]
        private ExportFactory<LecturesViewModel> LecturesViewModelFactory { get; set; }
        public LecturesViewModel LecturesViewModel
        {
            get { return LecturesViewModelFactory.CreateExport().Value; }
        }
    }
}
