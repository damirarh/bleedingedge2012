﻿using BE2012.ODataService;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE2012.Models
{
    [Export(typeof(ILecturesModel))]
    public class LecturesModel : ILecturesModel
    {
        private const string serviceRoot = "http://www.bleedingedge.si/dataservice.svc/";

        public virtual async Task<List<LectureInfo>> GetLectureInfos()
        {
            var context = new BleedingEdgeEntities(new Uri(serviceRoot));

            var query = context.Lectures.Expand(l => l.Lecturers);

            var result = await ExecuteAsync(query);

            return result.Select(l => new LectureInfo
                {
                    IdLecture = l.IdLecture,
                    Title = l.Title,
                    Lecturers = String.Join(", ", l.Lecturers.Select(p => String.Format("{0} {1}", p.Name, p.Surname))),
                    Description = l.Description,
                    DateStart = l.DateStart,
                    ImageUriString = String.Format("https://bleedingedgestorage.blob.core.windows.net/predavatelji-slike/{0}", l.Lecturers.Select(p => p.Image).FirstOrDefault())
                }).ToList();
        }

        private static async Task<IEnumerable<TResult>> ExecuteAsync<TResult>(DataServiceQuery<TResult> query)
        {
            var queryTask = Task.Factory.FromAsync<IEnumerable<TResult>>(query.BeginExecute(null, null),
                (queryAsyncResult) =>
                {
                    var results = query.EndExecute(queryAsyncResult);
                    return results;
                });

            return await queryTask;
        }
    }
}
