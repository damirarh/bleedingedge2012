﻿using BE2012.ODataService;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE2012.Models
{
    public class LectureInfo
    {
        public int IdLecture { get; set; }
        public string Title { get; set; }
        public string Lecturers { get; set; }
        public string Description { get; set; }
        public DateTime DateStart { get; set; }
        public string ImageUriString { get; set; }
        
        public Uri ImageUri 
        {
            get { return new Uri(ImageUriString); }
        }
    }
}
