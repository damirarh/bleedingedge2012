﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE2012.Models
{
    [Export(typeof(ILecturesModel))]
    public class DesignLecturesModel : ILecturesModel
    {
        public Task<List<LectureInfo>> GetLectureInfos()
        {
            var lectures = new List<LectureInfo>
            {
                new LectureInfo 
                { 
                    DateStart = DateTime.Today.AddHours(9), 
                    Description = "This planet has — or rather had — a problem, which was this: most of the people living on it were unhappy for pretty much all of the time.", 
                    IdLecture = 42, 
                    ImageUriString = "ms-appx:///Assets/Logo.png", 
                    Lecturers = "Arthur Dent", 
                    Title = "Encyclopaedia Galactica"
                },
                new LectureInfo 
                { 
                    DateStart = DateTime.Today.AddHours(9), 
                    Description = "Why is it that when one man builds a wall, the next man immediately needs to know what's on the other side?", 
                    IdLecture = 7, 
                    ImageUriString = "ms-appx:///Assets/Logo.png", 
                    Lecturers = "Ned Stark", 
                    Title = "Winter is coming"
                },
                new LectureInfo 
                { 
                    DateStart = DateTime.Today.AddHours(11), 
                    Description = "Think of Adam and Eve like an imaginary number, like the square root of minus one: you can never see any concrete proof that it exists, but if you include it in your equations, you can calculate all manner of things that couldn’t be imagined without it.", 
                    IdLecture = 3, 
                    ImageUriString = "ms-appx:///Assets/Logo.png", 
                    Lecturers = "Lyra Belacqua", 
                    Title = "Dust"
                }};

            return Task.Run<List<LectureInfo>>(() => lectures);
        }
    }
}
