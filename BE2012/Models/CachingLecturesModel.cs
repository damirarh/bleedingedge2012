﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE2012.Models
{
    [Export(typeof(ILecturesModel))]
    public class CachingLecturesModel : LecturesModel
    {
        private readonly SQLiteAsyncConnection db = new SQLiteAsyncConnection("cache.sqlite");

        public override async Task<List<LectureInfo>> GetLectureInfos()
        {
            await db.CreateTableAsync<LectureInfo>();

            var lectures = await db.Table<LectureInfo>().ToListAsync();
            if (lectures.Count == 0)
            {
                lectures = await base.GetLectureInfos();
                await db.InsertAllAsync(lectures);
            }

            return lectures;
        }
    }
}
