using BE2012.ODataService;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE2012.Models
{
    public interface ILecturesModel
    {
        Task<List<LectureInfo>> GetLectureInfos();
    }
}
